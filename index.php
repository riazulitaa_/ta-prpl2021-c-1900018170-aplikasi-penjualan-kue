<!DOCTYPE html>
<html>
<head>
	<title>Halaman Beranda</title>
	<!-- menghubungkan dengan file css -->
	<link rel="stylesheet" type="text/css" href="stylemenu.css">
	<!-- menghubungkan dengan file jquery -->
	<script type="text/javascript" src="jquery.js"></script>
</head>
<body bgcolor="#E6E6FA">
	<div class="content">
	<header>
		<h1 class="judul">RIA's CAKE</h1>
	</header>

	<div class="menu">
		<ul>
			<li><a href="index.php?page=home">BERANDA</a></li>
			<li><a href="produk.php?page=pdt">PRODUK</a></li>
			<li><a href="shop.php?page=cart">SHOPPING CART</a></li>
			<li><a href="pembayaran.php?page=bayar">PEMBAYARAN</a></li>
			<li><a href="kontak.php?page=cont">CONTACT US</a></li>
			<li><a href="logout.html">LOGOUT</a></li>
		</ul>
	</div>

	<div class="badan">
		
	<?php 
	if(isset($_GET['page'])){
		$page = $_GET['page'];

		switch ($page) {
			case 'home':
				include "halaman/beranda.php";
				break;
			case 'pdt':
				include "halaman/produk.php";
				break;
			case 'cart':
				include "halaman/shop.php";
				break;
			case 'bayar':
				include "halaman/pembayaran.php";
				break;	
			case 'cont':
				include "halaman/kontak.php";
				break;	
			default:
				echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
				break;
		}
	}
	else{
		include "halaman/beranda.php";
	}

	 ?>

	</div>
</div>
<br>
	<div class="footer">
      <footer><center>CopyRight&#169;Riazulita 2021 <a expr:href='data:blog.homepageUrl' rel='copyright'><data:blog.title/></a></center></footer>
</div>
</body>
</html>